package com.sterlingbankng.www.edward;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class AllFragment extends Fragment {


    public AllFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all, container, false);

        RecyclerView rvDeals = view.findViewById(R.id.rvTimeline);
        //final DealAdapter adapter = new DealAdapter();
        //rvDeals.setAdapter(adapter);
        LinearLayoutManager dealsLayoutManager = new LinearLayoutManager(this.getActivity(), RecyclerView.VERTICAL, false);
        rvDeals.setLayoutManager(dealsLayoutManager);
        //FirebaseUtil.attachListener();

        return view;
    }

}
